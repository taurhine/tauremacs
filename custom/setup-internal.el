;; https://gitlab.com/taurhine/tauremacs

;; set the flag to initialise external setup key-bindings later on
(setq internal-bindings t)

;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Emacs Customisations ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-theme 'wombat)

;;garbage collect when emacs losts focus
(add-hook 'focus-out-hook 'garbage-collect)
(if (display-graphic-p)
    (progn
      (menu-bar-mode -1)
      (tool-bar-mode -1)
      (scroll-bar-mode -1)))

(setq inhibit-startup-message t)
(column-number-mode)
(blink-cursor-mode 0)

;;wrap at word boundaries
(setq-default word-wrap t)

;; disable backup
(setq backup-inhibited t)
;; disable auto save
(setq auto-save-default nil)

(defalias 'yes-or-no-p 'y-or-n-p)

;;enable auto refresh all buffers when files have changed on disk
(global-auto-revert-mode t)

;; increase kill-ring capacity
(setq kill-ring-max 5000)

;;windmove shall use Meta key instead of shift
(windmove-default-keybindings 'meta)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;
;; Searching ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;override the default grep command
(setq grep-command "grep -Erin \"\" --include=*.{} .")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Setup Session Management ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Only visible buffers will be restored on startup
(desktop-save-mode 1)

;;kill all invisible buffers which contain no *in their name
(defun tp-kill-invisible-buffers ()
  "Kill all buffers which are not currently shown in any window."
  (interactive)
  (dolist (buf  (buffer-list))
    (unless (or (get-buffer-window buf 'visible) (string-suffix-p "*" (format "%s" buf)))
      (kill-buffer buf)))
  (message "Killed invisible buffers."))

;; On Exit clean up the buffers which are not in focus
(add-hook 'kill-emacs-hook 'tp-kill-invisible-buffers)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;
;; Line Editing ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun tp-copy-line()
  (interactive)
  (copy-region-as-kill (line-beginning-position) (line-end-position))
  )

(defun tp-duplicate-line()
  (interactive)
  (kill-whole-line)
  (yank)
  (yank)
  (backward-char)
  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;
;; Window Dedication ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun tp-kill-this-buffer ()
  (kill-buffer (current-buffer)))

(defun undedicate-window ()
  "Undedicate Window"
  (interactive)
  (let (window (get-buffer-window (current-buffer)))
    (set-window-dedicated-p window nil))
)

(defun undedicate-and-kill-this-buffer ()
  "Undedicate Window and kill this buffer"
  (interactive)
  (undedicate-and-do 'tp-kill-this-buffer)
  )

(defun undedicate-and-do (arg)
  "Undedicate Window and run the given command"
  (interactive)
  (undedicate-window)
  (funcall arg)
  )

(defun toggle-window-dedicated ()
  "Toggle whether the current active window is dedicated or not"
  (interactive)
  (message
   (if (let (window (get-buffer-window (current-buffer)))
         (set-window-dedicated-p window
                                 (not (window-dedicated-p window))))
       "Window '%s' is dedicated"
     "Window '%s' is normal")
   (current-buffer)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Editing and Coding Style ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; use space to indent by default
(setq-default indent-tabs-mode nil)

;; set appearance of a tab that is represented by 3 spaces
(setq-default tab-width 3)

(setq global-mark-ring-max 5000         ; increase mark ring to contains 5000 entries
      mark-ring-max 5000                ; increase kill ring to contains 5000 entries
      mode-require-final-newline t      ; add a newline to end of file
      tab-width 3                       ; default to 3 visible spaces to display a tab
      )

(add-hook 'sh-mode-hook (lambda () (setq tab-width 3)))
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)
(delete-selection-mode)
(global-set-key (kbd "RET") 'newline-and-indent)
(setq c-default-style "linux") ;; set style to "linux"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;
;; Associations ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;open inc files in asm-mode
(add-to-list 'auto-mode-alist '("\\.inc\\'" . asm-mode))
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'setup-cedet)

(provide 'setup-internal)
;;; setup-internal.el ends here
