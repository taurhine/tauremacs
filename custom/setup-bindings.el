;; https://gitlab.com/taurhine/tauremacs

(defun tp-py-proper-region-indent(START END &optional COLUMN)
  ;;(interactive)
  (python-indent-shift-right (region-beginning) (region-end))

  ;; for whatever reason indent-for-tab-command unmarks after calling the indent-region-function
  ;; following lines undo that unmark so that it is consistent with dedent behaviour
  (set-mark-command)
  (pop-to-mark-command)
)

;;set indent-region-function for python
(defun tp-py-set-indent-region-function ()
  (setq indent-region-function 'tp-py-proper-region-indent)
  ;;(setq indent-region-function 'python-indent-shift-right)
  )

(add-hook 'python-mode-hook 'tp-py-set-indent-region-function)

(defun tp-py-proper-region-dedent()
  (interactive)
  (if (use-region-p)
      (python-indent-shift-left (region-beginning) (region-end))
    )
)

;; remap the binding used by python-indent-dedent-line with tp-py-proper-region-dedent
(define-key python-mode-map [remap python-indent-dedent-line] 'tp-py-proper-region-dedent)

(defun undedicate-and-helm-mini ()
     "Undedicate window and call helm-mini."
     (interactive)
     (undedicate-window)
     (helm-mini)
     )

(defun bindings-for-internal ()
  (global-set-key [C-d]    'tp-duplicate-line)
  (global-set-key [C-k]    'kill-whole-line)
  (global-set-key [C-l]    'tp-copy-line)

  (global-set-key [M-up]    'windmove-up)
  (global-set-key [M-down]  'windmove-down)
  (global-set-key [M-left]  'windmove-left)
  (global-set-key [M-right] 'windmove-right)

  (global-set-key [f8]    'undedicate-and-kill-this-buffer)
)


(defun bindings-for-external ()

(bind-keys*
 ("C-d" . tp-duplicate-line)
 ("C-k" . kill-whole-line)
 ("C-l" . tp-copy-line)

 ;; switching focus between windows
 ("<M-up>" . windmove-up)
 ("<M-down>" . windmove-down)
 ("<M-left>" . windmove-left)
 ("<M-right>" . windmove-right)

 ;; moving buffers around
 ("<M-S-up>" . buf-move-up)
 ("<M-S-down>" . buf-move-down)
 ("<M-S-left>" . buf-move-left)
 ("<M-S-right>" . buf-move-right)

 ("<f3>" . helm-projectile-grep)        ;grep from the project root
 ("<S-f3>" . grep)
 ("<M-f3>" . helm-grep-do-git-grep)     ;grep from the git repo root

 ("<f4>" . helm-do-ag-project-root)
 ("<S-f4>" . helm-do-grep-ag)

 ;; buffer control group
 ;; helm-mini is supposed to modify the current buffer thus undedicate first
 ("<f5>" . undedicate-and-helm-mini)
 ("<S-f5>" . helm-bookmarks)            ;just needed to add bookmarks, to select bookmarks use helm-mini

 ("<f6>" . projectile-find-file)
 ("<S-f6>" . helm-gtags-find-tag)
 ("<f7>" . helm-find-files)

 ;; kill-this-buffer is supposed to modify the current buffer thus undedicate first
 ("<f8>" . undedicate-and-kill-this-buffer)
 ("<S-f8>" . projectile-kill-buffers)   ;kill project specific buffers
 ("<M-f8>" . tp-kill-invisible-buffers)

 ("<f9>" . zygospore-toggle-delete-other-windows)
 ("<f10>" . split-window-vertically)
 ("<f11>" . split-window-horizontally)
 ("<f12>" . delete-window)

 ;;replacing i-search with helm-occur
 ;;("C-s" . helm-occur)

 ;;replacing helm-occur with helm-swoop
 ("C-s" . helm-swoop-without-pre-input)

 ;;Apply to all opened buffers of the current project
 ("C-M-s" . helm-multi-swoop-projectile)

 ;; Useful bindings
 ;; M-m to select multiple candidates in buffer list or while kill list
 ;; C-<space> to select one by one
 ;; C-x k, M-m then M-d to kill multiple buffers
 ("M-d" . helm-buffer-run-kill-buffers)

 ("M-x" . helm-M-x)
 ("M-y" . helm-show-kill-ring)
 ("C-x C-f" . helm-find-files)
 ("C-:" . anzu-query-replace)
 ("C-M-:" . anzu-query-replace-regexp)
 ("C-;" . iedit-mode)
 ("M-i" . helm-semantic-or-imenu)
 ("M-;" . comment-dwim-2) ; comment/uncomment lines, regions and add comments at the end of the line
 ("M-:" . comment-box)
 ("C-<f1>" . magit-status)
 ("M-<f1>" . magit-log-head)
 ("S-<f1>" . magit-file-log)
 ("<pause>" . toggle-window-dedicated)
 )

(global-set-key [C-f2]    'projectile-compile-project)
(global-set-key [C-f3]    'gud-reset)
(global-set-key [C-f4]    'gud-run)
(global-set-key [C-f5]    (lambda() (interactive) (function-with-refresh #'gud-step)))

(global-set-key [C-f6]    (lambda() (interactive) (function-with-refresh #'gud-next)))
(global-set-key [C-f7]    (lambda() (interactive) (function-with-refresh #'gud-finish)))
(global-set-key [C-f8]    'gud-cont)
(global-set-key [C-S-f8]  'gud-until)
(global-set-key [C-f9]    'gud-break)
(global-set-key [C-S-f9]  'gud-remove)
(global-set-key [C-f11]   'gud-watch)

;; Global Remap
(define-key global-map [remap find-tag] 'helm-gtags-select)
(define-key global-map [remap list-buffers] 'helm-buffers-list)

;; Global Unset
(global-unset-key (kbd "C-x c"))

;; key bindings
(with-eval-after-load 'helm-gtags
  (define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
  (define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
  (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
  (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
  (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
  (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history))
)

(if internal-bindings
    (bindings-for-internal)
  (bindings-for-external)
  )

(provide 'setup-bindings)
